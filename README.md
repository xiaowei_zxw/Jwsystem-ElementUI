# 基于springboot的教务管理系统 - 前端

author:zxw

email:502513206@qq.com

@ Jishou University 

------

# 1.前言

项目vue版本为3.0以上，如果无法正常使用请升级webpack以及vue-cli相关版本

本项目为开源项目，不可用作其它用途，麻烦给作者一个star啦，谢谢。

# 2.项目运行效果

5.2.2 界面设计

![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/145822_54362b89_4926334.jpeg "wps10.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/145840_e0ae20ce_4926334.jpeg "wps12.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/145853_e3753c91_4926334.jpeg "wps18.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0126/145904_ae527c83_4926334.jpeg "wps19.jpg")
